package com.gsharini.app;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;
//import static org.junit.jupiter.api.Assertions.ass

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void testBooleanPositive() {
        Helloworld helloworld = new Helloworld();
        assertTrue(helloworld.returnBool(1));
    }

    @Test
    public void testBooleanNegative() {
        Helloworld helloworld = new Helloworld();
        assertTrue(helloworld.returnBool(-1));
    }

    @Test
    public void testBooleanZero() {
        Helloworld helloworld = new Helloworld();
        assertTrue(!helloworld.returnBool(0));
    }


}
